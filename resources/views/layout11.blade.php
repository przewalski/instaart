<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">

    <title>instaArt</title>
    <link rel="stylesheet" href="{{ env('APP_URL') }}css/homepage.css">
    <link rel="stylesheet" href="{{ env('APP_URL') }}css/app.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>

@if(Session::has('flash_message'))
    <div class="alert alert-danger">
        <button type="button" class="close" date-dismiss="alert" aria-hidden="true"></button>
        {{Session::get('flash_message')}}
    </div>
@endif
@if(Session::has('flash_message_important'))
    <div class="alert alert-success">
        <button type="button" class="close" date-dismiss="alert" aria-hidden="true"></button>
        {{Session::get('flash_message_important')}}
    </div>
@endif

<header>
    <div class="container-fluid">
        <div class="logo">
            <a href="{{ env('APP_URL') }}">
                <img src="{{ env('APP_URL') }}images/logo.svg" alt="instaArt">
            </a>
        </div>
        <nav>
            @if(Auth::guest())
                <ul>
                    <li><a style="color: black" href="{{ env('APP_URL') }}login">Login</a></li>
                    <li><a style="color: black" href="{{ env('APP_URL') }}register">Registration</a></li>
                    <li><a style="color: black" href="{{ env('APP_URL') }}upload">Upload</a></li>
                </ul>
            @else
                <ul>
                    <li><a style="color: black" href="{{ env('APP_URL') }}profile">Profile</a></li>
                    <li><a style="color: black" href="{{ env('APP_URL') }}upload">Upload</a></li>
                    <li><a style="color: black" href="{{ env('APP_URL') }}logout">Log out</a></li>
                </ul>
            @endif
        </nav>
    </div>
</header>

@yield('content')

<footer>
    <p>InstaArt &copy; 2019</p>
</footer>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>