@extends('layout11')

@section('content')

    <div id="personalProfileForm">
        <h1 id="username">{{$user->name}}</h1>

        <form action="{{ route('profile-update')}}" method="post">
            @csrf

            <a class="change">Change your username</a>
            <div class="content">
                <input type="text" name="name" value="{{$user->name}}">
                <button type="submit" style="margin: auto">Confirm</button>
            </div>

            <a class="change">Change your password</a>
            <div class="content" >
                <input type="password" name="password" placeholder="Your new password">
                <button type="submit" style="margin: auto">Confirm</button>
            </div>

        </form>
    </div>

    <button class="change" style="margin: auto">Mano skelbimai</button>
    @if(count($adverts))
    <div class="content">
        @foreach($adverts as $advert)
            <section>
                <div class="box">
                    <a href="{{ env('APP_URL') . $advert->id}}/edit"><img class="photo" src="{{ env('APP_URL') . 'uploads\\' . $advert->file_name}}" alt="Photo"></a>
                    <ul>
                        @if($advert->place)
                            <li><img src="{{ env('APP_URL') }}images/my_location-24px.svg" alt="">{{$advert->place}}</li>
                        @endif
                    </ul>
                </div>
            </section>
        @endforeach
    </div>
    @else
        <div class="content">
            <section>
                <div class="box" style="text-align: center; margin-top: 10px">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Klaida!</strong> Jūs neturite skelbimų!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </section>
        </div>
        @endif

    <script type="text/javascript">

        var coll = document.getElementsByClassName("change");
        for (let i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.maxHeight) {
                    content.style.maxHeight = null;
                }
                else {
                    content.style.maxHeight = content.scrollHeight + "px";
                }
            });
        }
    </script>
@endsection