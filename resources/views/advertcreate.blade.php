@extends('layout11')

@section('content')

    <section>
        <div class="box">
            <form action="store" method="post" enctype="multipart/form-data">
                @csrf
                    <label class="custom-upload" for="file_name" style="cursor: pointer;"><p>Click here to select a photo</p>
                        <input type="file" class="form-control-file @error('file_name') is-invalid @enderror" name="file_name" id="file_name" aria-describedby="fileHelp" onchange="loadFile(event)" style="display: none;">
                        <img class="photoUpload" id="output">

                        @error('file_name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </label>

                    <div class="location">
                        <input type="text" class="form-control @error('place') is-invalid @enderror" name="place" placeholder="Location">
                        <img src="{{ env('APP_URL') }}images/my_location-24px.svg" alt="icon">

                        @error('place')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                <button type="submit" class="btn btn-primary">UPLOAD</button>
            </form>
        </div>
    </section>

    <script>
        var loadFile = function(event) {
            var image = document.getElementById('output');
            image.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>

@endsection