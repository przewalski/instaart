@extends('layout11')

@section('content')

    @foreach($adverts as $advert)
    <!-- Button trigger modal -->
    <section>
        <div class="box">
            <img class="photo" src="{{ env('APP_URL') . 'uploads\\' . $advert->file_name}}" alt="Photo">
            <ul>
                <li><a href="{{ env('APP_URL') }}profile/{{$advert->user_id}}">{{$advert->user->name}}</a></li>
                @if($advert->place)
                    <li><img src="{{ env('APP_URL') }}images/my_location-24px.svg" alt="">{{$advert->place}}</li>
                @endif

                @if(Auth::guest())
                    <li>
                        <button type="button" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#exampleModalScrollable{{$advert->id}}">
                            Skaityti komentarus
                        </button>
                    </li>
                    @else
                    <li>
                        <button type="button" class="btn btn-outline-success btn-sm" data-toggle="modal" data-target="#exampleModalScrollable{{$advert->id}}">
                            Komentuoti
                        </button>
                    </li>
                    @endif
            </ul>
        </div>
    </section>

    <!-- Modal -->
    <section>
        <div class="box">
            <div class="modal fade" id="exampleModalScrollable{{$advert->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                                    <img class="photo" src="{{ env('APP_URL') . 'uploads\\' . $advert->file_name}}" alt="Photo">
                                    <ul>
                                        <li><a href="{{ env('APP_URL') }}profile/{{$advert->user_id}}">{{$advert->user->name}}</a></li>
                                        @if($advert->place)
                                            <li><img src="{{ env('APP_URL') }}images/my_location-24px.svg" alt="">{{$advert->place}}</li>
                                        @endif
                                    </ul>
                            @comments(['model' => $advert])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endforeach

@endsection