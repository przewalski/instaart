<?php


Route::get('','HomepageController@index');
Route::get('profile','HomepageController@profile');
Route::post('profile','HomepageController@updateProfile')->name('profile-update');

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout');

Route::get('upload','AdvertController@upload');
Route::post('store', 'AdvertController@store');
Route::post('{id}', 'AdvertController@update');

Route::get('{id}/edit', 'AdvertController@edit');

Route::get('profile/{id}', 'HomepageController@show');

Route::post('comments', '\Laravelista\Comments\CommentController@store');
Route::delete('comments/{comment}', '\Laravelista\Comments\CommentController@destroy');
Route::put('comments/{comment}', '\Laravelista\Comments\CommentController@update');
Route::post('comments/{comment}', '\Laravelista\Comments\CommentController@reply');