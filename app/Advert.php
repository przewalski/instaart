<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;

class Advert extends Model
{
    use Commentable;

    protected $table = 'adverts';

    protected $fillable =
        [
            'user_id',
            'place',
            'file_name'
        ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
