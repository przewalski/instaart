<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Http\Requests\EditAdvertRequest;
use App\User;
use App\Http\Requests\CreateAdvertRequest;
use Illuminate\Http\Request;
use Auth;

class AdvertController extends Controller
{
    public function upload()
    {
        if (Auth::user())
        {
            return view('advertcreate');
        }
        else
        {
            \Session::flash('flash_message', 'Please login!');
            return redirect('login');
        }
    }

    public function store(CreateAdvertRequest $request)
    {
        $user = Auth::user();
        request()->validate([
            'file_name'  => 'required|mimes:jpg,jpeg,png|max:8192',
        ]);

        if ($files = $request->file('file_name')) {
            $path = $request->file('file_name')->store('');
        }

        Advert::create([
            'user_id' => $user->id,
            'file_name' => $path,
            'place' => $request->place
        ]);

        return redirect('');
    }

    public function edit($id)
    {
        $advert = Advert::findOrFail($id);
        if(Auth::id() == $advert->user_id)
            return view('editAdvert',compact('advert'));
        else return redirect("");
    }

    public function update($id, EditAdvertRequest $request)
    {
        $advert = Advert::findOrFail($id);

        if( $request->file_name === NULL && $request->place === $advert->place)
            return redirect('profile');
        else if($request->file_name === NULL) {
            $advert->update($request->all());
            return redirect('profile');
        }
        
        request()->validate([
            'file_name'  => 'required|mimes:jpg,jpeg,png|max:4096',
        ]);

        if ($files = $request->file('file_name')) {
            $path = $request->file('file_name')->store('');
        }

        $advert->update([
            'file_name' => $path,
            'place' => $request->place
        ]);

        return redirect('profile');
    }

}
