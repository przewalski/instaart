<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator,Redirect,Response,File;
Use App\Document;

class FileController extends Controller
{
    // tutaj pokazuje strona formy
    public function index()
    {
        $documents = Document::all();

        return view('file', compact('documents'));
    }

    // tutaj zapisuje sie forma przy nacisnieciu na Submit
    public function save(Request $request)
    {
        // sprawdzenie typu fajlu (jpg/png..., rozmiar do 4mb)
        request()->validate([
            'file'  => 'required|mimes:jpg,jpeg,png,gif|max:4096',
        ]);

        if ($files = $request->file('file')) {
            $path = $request->file('file')->store('');
        }

        Document::create([
            'name' => $path
        ]);

        return Redirect::to("file")
            ->withSuccess('Great! file has been successfully uploaded.');

    }
}