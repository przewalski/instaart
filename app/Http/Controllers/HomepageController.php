<?php

namespace App\Http\Controllers;

use App\Advert;
use App\User;
use Auth;
use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index()
    {
        $adverts = Advert::all();
        $users = User::all();

        return view('homepage',compact('adverts', 'users'));
    }

    public function profile()
    {
        $user = Auth::user();
        if(!Auth::guest())
        {
            $adverts = Advert::where('user_id', '=', $user->id)->get();
            return view('profile',compact('user','adverts'));
        }
        else{
            return redirect('login');
        }
    }

    public function updateProfile(ProfileUpdateRequest  $request)
    {
        $user = Auth::user();
        $user->update([
            'name' => $request->name
        ]);

        if (!empty($request->password)) {
            $user->update([
                'password' => bcrypt($request->password)]
            );
        }

        return redirect('profile');
    }


    public function show($id)
    {
        $user = User::findOrFail($id);
        $user = $user['name'];
        $adverts = Advert::where('user_id', '=', $id)->get();
        return view('profileShow',compact('adverts', 'user'));
    }
}
